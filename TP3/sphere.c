#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mt19937-64.h"
#include "sphere.h"

// ----------------------------- simSphere ---------------------------- //
//                                                                      //
// Entrée : un long integer nbPoints qui correspond au nombre de        //
// points souhaités pour la simulation.                                 //
//                                                                      //
// Sortie : une estimation du volume de la sphère.                      //
//                                                                      //
// Objectif : simSphere simule le volume d'une sphère à l'aide de la    //
// méthode de Monte Carlo, qui consiste à générer un grand nombre de    //
// points aléatoires dans un cube englobant la sphère, puis à estimer   //
// le volume de la sphère en fonction de la proportion de points qui    //
// tombent à l'intérieur de la sphère.                                  //
// -------------------------------------------------------------------- //

double simSphere(long int nbPoints)
{
    int      i;
    long int inside = 0;
    double   estimate;
    double   x;
    double   y;
    double   z;

    for (i = 0; i < nbPoints; i++)
    {
        x = genrand64_real1() * 2 - 1; // nombre aléatoire entre -1 et 1
        y = genrand64_real1() * 2 - 1; // nombre aléatoire entre -1 et 1
        z = genrand64_real1() * 2 - 1; // nombre aléatoire entre -1 et 1

        // on regarde si le point se trouve dans la sphère
        if ((x * x) + (y * y) + (z * z) <= 1)
        {
            inside++;
        }
    }

    // Estimation du volume de la sphère
    estimate = 8.0 * ((double)inside) / ((double)nbPoints);

    return estimate;
}

// ------------------------------ average ----------------------------- //
//                                                                      //
// Entrée : un tableau de double results qui contient le résultat de    //
// plusieurs expériences, et n la taille du tableau.                    //
//                                                                      //
// Sortie : La moyenne de type double des valeurs contenues dans le     //
// tableau.                                                             //
//                                                                      //
// Objectif : L'objectif de cette fonction est de fournir la moyenne    //
// d'une série de valeurs numériques stockées dans le tableau.          //
// -------------------------------------------------------------------- //

double average(double *results, int n)
{
    int    i;
    double total = 0;

    for (i = 0; i < n; i++)
    {
        total = total + results[i];
    }

    return total / n;
}

// ----------------------------- variance ----------------------------- //
//                                                                      //
// Entrée : un tableau de double results qui contient le résultat de    //
// plusieurs expériences, et n la taille du tableau.                    //
//                                                                      //
// Sortie : La variance de type double des valeurs contenues dans le    //
// tableau.                                                             //
//                                                                      //
// Objectif : L'objectif de cette fonction est de fournir la variance   //
// empirique d'une série de valeurs numériques stockées dans le         //
// tableau.                                                             //
// -------------------------------------------------------------------- //

double variance(double *results, int n)
{
    int    i;
    double total = 0;
    double avg = average(results, n);

    for (i = 0; i < n; i++)
    {
        total += pow(results[i] - avg, 2);
    }

    total /= n - 1;

    return total;
}

// ----------------------- simSphereExperiments ----------------------- //
//                                                                      //
// Entrée : un nombre de points nbPoints de type long int,              //
// correspondant au nombre de points souhaité par expérience, et un     //
// integer nbExp correspondant au nombre d'expériences souhaité.        //
//                                                                      //
// Sortie : Un tableau de double contenant le résultat de chaque        //
// expérience.                                                          //
//                                                                      //
// Objectif : L'idée est de réaliser plusieurs expériences              //
// indépendantes pour estimer le volume d'une sphère en utilisant la    //
// méthode de Monte Carlo. On lance plusieurs fois simSphere et l'on    //
// retourne un tableau contenant les résultats de chaque expérience.    //
// -------------------------------------------------------------------- //

double *simSphereExperiments(long int nbPoints, int nbExp)
{
    int                i;

    // Seule et unique initialisation
    unsigned long long init[4] = {0x12345ULL, 0x23456ULL, 0x34567ULL, 0x45678ULL}; // Tab pour initialisation de MT
    int                l = 4;
    init_by_array64(init, l);


    double            *results = malloc(nbExp * sizeof(double));

    if (results == NULL)
    {
        perror("erreur allocation mémoire simSphereExperiments");
    }

    for (i = 0; i < nbExp; i++)
    {
        results[i] = simSphere(nbPoints);
    }

    return results;
}

// ------------------------- simSphereAverage ------------------------- //
//                                                                      //
// Entrée : un nombre de points nbPoints de type long int,              //
// correspondant au nombre de points souhaité par expérience, et un     //
// integer nbExp correspondant au nombre d'expériences souhaité.        //
//                                                                      //
// Sortie : Un double correspondant à la moyenne de toutes les          //
// estimations du volume de la sphère obtenues à partir des             //
// différentes expériences.                                             //
//                                                                      //
// Objectif : Cette fonction exécute plusieurs simulations              //
// indépendantes du volume de la sphère (via simSphereExperiments),     //
// puis calcule et retourne la moyenne des résultats obtenus. Le but    //
// est d'obtenir une estimation plus précise du volume en moyennant     //
// les résultats de plusieurs expériences.                              //
// -------------------------------------------------------------------- //

double simSphereAverage(long int nbPoints, int nbExp)
{
    double *experiment = simSphereExperiments(nbPoints, nbExp);
    double  avg = average(experiment, nbExp);
    free(experiment);
    return avg;
}

// -------------------- radiusConfidenceInterval ---------------------- //
//                                                                      //
// Entrée : un tableau de double results correspondant à des résultats  //
// d'expériences, et un integer nbExp correspondant au nombre           //
// d'expériences.                                                       //
//                                                                      //
// Sortie : Un double correspondant au rayon de confiance calculé à     //
// l'aide des valeurs des différentes expériences.                      //
//                                                                      //
// Objectif : Cette fonction calcule le rayon de l'intervalle de        //
// confiance à un certain niveau (en utilisant la valeur t de la        //
// distribution de Student) pour les résultats des expériences. Le      //
// rayon est calculé en fonction de la variance des résultats et du     //
// nombre d'expériences. Cela permet de déterminer la précision de      //
// l'estimation moyenne obtenue à partir des différentes expériences.   //
// -------------------------------------------------------------------- //

double radiusConfidenceInterval(double *results, int nbExp)
{
    return T_STUDENT * (sqrt(variance(results, nbExp)) / sqrt(nbExp));
}

// ----------------------- confidenceInterval ------------------------- //
//                                                                      //
// Entrée : un nombre de points nbPoints de type long int,              //
// correspondant au nombre de points souhaité par expérience, et un     //
// integer nbExp correspondant au nombre d'expériences souhaité.        //
//                                                                      //
// Sortie : Un tableau contenant 2 doubles qui sont les bornes          //
// inférieures et supérieures de l'intervalle de confiance.             //
//                                                                      //
// Objectif : Cette fonction réalise plusieurs simulations indépendantes//
// pour estimer le volume d'une sphère avec la méthode de Monte Carlo.  //
// Elle calcule ensuite un intervalle de confiance autour de la moyenne //
// des estimations obtenues.                                            //
// -------------------------------------------------------------------- //

double *confidenceInterval(long int nbPoints, int nbExp)
{
    double *result = malloc(2 * sizeof(double));
    double *experiments;
    double  avg;
    double  radius;

    if (result == NULL)
    {
        perror("erreur malloc confidenceInterval");
    }

    experiments = simSphereExperiments(nbPoints, nbExp);
    avg = average(experiments, nbExp);
    radius = radiusConfidenceInterval(experiments, nbExp);

    result[0] = avg - radius;
    result[1] = avg + radius;

    free(experiments);

    return result;
}