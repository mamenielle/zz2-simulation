#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mt19937-64.h"
#include "sphere.h"

int main()
{
    unsigned long long init[4] = {0x12345ULL, 0x23456ULL, 0x34567ULL, 0x45678ULL}; // Tab pour initialisation de MT
    int l = 4;
    init_by_array64(init, l);

    FILE *f = fopen("Résultats simulation sphère", "w+");

    fprintf(f, "résultats de simSphere\n"); // résultat attendu pour r = 1 : 4.1887
    fprintf(f, "* pour 1 000 points : %lf\n", simSphere(1000));
    fprintf(f, "* pour 1 000 000 points : %lf\n", simSphere(1000000));
    fprintf(f, "* pour 1 000 000 000 points : %lf\n", simSphere(1000000000));

    fprintf(f, "résultats de simSphereAverage\n");
    fprintf(f, "* 10 essais : %lf %lf %lf\n", simSphereAverage(1000, 10), simSphereAverage(1000000, 10), simSphereAverage(1000000000, 10));
    fprintf(f, "* 40 essais : %lf %lf\n", simSphereAverage(1000, 40), simSphereAverage(1000000, 40));

    double *interval = confidenceInterval(1000000, 40);
    fprintf(f, "l'intervalle de confiance obtenu pour 1 000 000 points et 40 essais est de :\n");
    fprintf(f, "[%lf, %lf]\n", interval[0], interval[1]);

    free(interval);
    fclose(f);
    return 0;
}