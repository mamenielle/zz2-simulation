#define T_STUDENT 2.7045 // valeur à lire dans la table de student pour n = 40 et alpha = 0.01

double simSphere(long int nbPoints);
double average(double *results, int n);
double variance(double *results, int n);
double *simSphereExperiments(long int nbPoints, int nbExp);
double simSphereAverage(long int nbPoints, int nbExp);
double radiusConfidenceInterval(double *results, int nbExp);
double *confidenceInterval(long int nbPoints, int nbExp);