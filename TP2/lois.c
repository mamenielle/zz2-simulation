#include "lois.h"
#include "mt19937-64.c"

// -------------------------------------------------------------------- //
//                                poisson                               //
//                                                                      //
// Entrée : un flottant double lambda correspondant à la moyenne        //
// théorique du processus de Poisson.                                   //
//                                                                      //
// Sortie : un entier représentant le nombre d'événements générés       //
// selon la distribution de Poisson.                                    //
//                                                                      //
// Description :                                                        //
// Cette fonction utilise le principe de génération de Poisson pour     //
// produire un nombre entier aléatoire basé sur la moyenne lambda.      //
//                                                                      //
// -------------------------------------------------------------------- //

int poisson(double lambda)
{
    double L = exp(-lambda); // Calcul du seuil
    double p = 1.0;          // Produit des nombres aléatoires
    int    k = 0;            // Compteur d'événements

    while (p > L)
    {
        k++;                    // Incrémente le nombre d'événements
        p *= genrand64_real3(); // Multiplie par un nombre aléatoire de ]0, 1[.
    }

    return k - 1;
}

// -------------------------------------------------------------------- //
//                          poissonSimulation                           //
//                                                                      //
// Entrée :                                                             //
// - double lambda : moyenne théorique pour la loi de Poisson.          //
// - char *fichier : chemin vers le fichier où les résultats seront     //
//   enregistrés.                                                       //
//                                                                      //
// Sortie :                                                             //
// - Aucune (les résultats sont écrits dans un fichier).                //
//                                                                      //
// Description :                                                        //
// Cette fonction simule des échantillons d'une loi de Poisson en       //
// générant N_ECHANTILLONS échantillons selon la moyenne lambda.        //
// Les occurrences de chaque valeur sont comptées et enregistrées       //
// dans un tableau. Ensuite, les résultats sont écrits dans le fichier  //
// donné en paramètre.                                                  //
// -------------------------------------------------------------------- //

void poissonSimulation(double lambda, char *fichier)
{
    int i;
    int tabPoisson[N_ECHANTILLONS];      // Stocker les valeurs de la simulation de la loi de poisson
    int occurrences[POISSON_N_OCCURENCES] = {0}; // Tableau pour compter les occurrences

    // Remplir le tableau avec des échantillons de la loi de Poisson
    for (i = 0; i < N_ECHANTILLONS; i++)
    {
        tabPoisson[i] = poisson(lambda);
        if (tabPoisson[i] < POISSON_N_OCCURENCES)
        {
            occurrences[tabPoisson[i]]++;
        }
    }

    // Écrire les résultats dans un fichier
    FILE *file = fopen(fichier, "w+");
    if (file == NULL)
    {
        perror("Erreur d'ouverture du fichier");
        return;
    }

    for (i = 0; i < POISSON_N_OCCURENCES; i++)
    {
        fprintf(file, "%d %d\n", i, occurrences[i]); // Écrire le nombre d'événements et son occurrence
    }

    fclose(file);
}

// -------------------------------------------------------------------- //
//                            log_normale                               //
//                                                                      //
// Entrée :                                                             //
// - double mu : moyenne de la distribution normale.                    //
// - double sigma : écart type de la distribution normale.              //
//                                                                      //
// Sortie :                                                             //
// - double : un échantillon généré suivant une loi log-normale.        //
//                                                                      //
// Description :                                                        //
// On génère un échantillon suivant une loi log-normale en utilisant    //
// la méthode de Box-Muller pour transformer des nombres aléatoires     //
// uniformes en une variable normale standard. On utilise ensuite l'    //
// exponentielle sur la variable normale pour obtenir la variable       //
// log-normale.                                                         //
//                                                                      //
// -------------------------------------------------------------------- //

double log_normale(double mu, double sigma)
{
    // Générer deux nombres aléatoires uniformes u1 et u2 dans l'intervalle ]0, 1[
    double u1 = genrand64_real3();
    double u2 = genrand64_real3();

    // Transformer u1 et u2 en une variable normale standard avec Box-Muller
    double z = sqrt(-2.0 * log(u1)) * cos(2.0 * M_PI * u2);

    // Transformer en une variable log-normale
    double x = exp(mu + sigma * z);

    return x;
}

// -------------------------------------------------------------------- //
//                       simulationLogNormale                           //
//                                                                      //
// Entrée :                                                             //
// - double mu : moyenne de la distribution log-normale.                //
// - double sigma : écart type de la distribution log-normale.          //
// - char *fichier : chemin vers le fichier où les résultats seront     //
//   enregistrés.                                                       //
//                                                                      //
// Sortie :                                                             //
// - Aucune (les résultats sont écrits dans un fichier).                //
//                                                                      //
// Description :                                                        //
// Cette fonction simule des échantillons suivant une loi log-normale   //
// en générant N_ECHANTILLONS échantillons selon les paramètres mu et   //
// sigma. Les échantillons sont regroupés en classes et les fréquences  //
// sont comptées. Les résultats sont écrits dans un fichier spécifié    //
// sous la forme : centre de classe - fréquence.                        //
//                                                                      //
// -------------------------------------------------------------------- //

void simulationLogNormale(double mu, double sigma, char *fichier)
{
    int    i;

    // Valeurs choisies arbitrairement pour la plage de données
    double min_value = 0;
    double max_value = 3;

    int    classes[LOGNORMALE_N_CLASSES] = {0};
    int    classIndex;
    double x;
    double classCenter;

    FILE *file = fopen(fichier, "w+");
    if (file == NULL)
    {
        printf("Erreur d'ouverture du fichier.\n");
        return;
    }

    // Simuler les échantillons
    for (i = 0; i < N_ECHANTILLONS; i++)
    {
        x = log_normale(mu, sigma); // Obtenir une valeur selon la loi log-normale

        // Trouver la classe de valeur correspondante
        if (x >= min_value && x < max_value)
        {
            classIndex = (int)((x - min_value) / ((max_value - min_value) / LOGNORMALE_N_CLASSES));
            if (classIndex >= 0 && classIndex < LOGNORMALE_N_CLASSES)
            {
                classes[classIndex]++;
            }
        }
    }

    // Écrire les résultats dans le fichier
    for (i = 0; i < LOGNORMALE_N_CLASSES; i++)
    {
        classCenter = min_value + (i + 0.5) * ((max_value - min_value) / LOGNORMALE_N_CLASSES);
        fprintf(file, "%lf %d\n", classCenter, classes[i]); // Écrire le centre de la classe et sa fréquence
    }

    fclose(file);
}

// -------------------------------------------------------------------- //
//                                cauchy                                //
//                                                                      //
// Entrée :                                                             //
// - double x0 : paramètre de localisation de la loi de Cauchy.         //
// - double gamma : paramètre d'échelle de la loi de Cauchy.            //
//                                                                      //
// Sortie :                                                             //
// - double : une valeur aléatoire générée selon la loi de Cauchy.      //
//                                                                      //
// Description :                                                        //
// Cette fonction génère une valeur aléatoire suivant une loi de        //
// Cauchy en utilisant la transformation d'une variable uniforme.       //
//                                                                      //
// -------------------------------------------------------------------- //

double cauchy(double x0, double gamma)
{
    return x0 + gamma * tan(M_PI * (genrand64_real3() - 0.5)); // Tirage d'une valeur selon la loi de Cauchy
}

// -------------------------------------------------------------------- //
//                             simulationCauchy                         //
//                                                                      //
// Entrée :                                                             //
// - double x0 : paramètre de localisation de la loi de Cauchy.         //
// - double gamma : paramètre d'échelle de la loi de Cauchy.            //
// - char *fichier : chemin vers le fichier où les résultats seront     //
//   enregistrés.                                                       //
//                                                                      //
// Sortie :                                                             //
// - Aucune (les résultats sont écrits dans un fichier).                //
//                                                                      //
// Description :                                                        //
// Cette fonction simule des échantillons suivant une loi de Cauchy     //
// en générant N_ECHANTILLONS échantillons selon les paramètres x0 et   //
// gamma. Les échantillons sont regroupés en classes et les fréquences  //
// sont comptées. Les résultats sont ensuite écrits dans un fichier.    //
//                                                                      //
// -------------------------------------------------------------------- //

void simulationCauchy(double x0, double gamma, char *fichier)
{
    int    i;

    // pour de meilleurs observations on centre en x0
    int    min_value = x0 - CAUCHY_LARGEUR;
    int    max_value = x0 + CAUCHY_LARGEUR;

    // Tableau pour compter le nombre d'occurrences dans chaque classe
    int    classes[CAUCHY_N_CLASSES] = {0};
    int    classIndex;
    double x;
    double classCenter;
    
    FILE *file = fopen(fichier, "w+");
    if (file == NULL)
    {
        printf("Erreur d'ouverture du fichier.\n");
        return;
    }

    for (i = 0; i < N_ECHANTILLONS; i++)
    {
        x = cauchy(x0, gamma); // Obtenir une valeur selon la loi de Cauchy

        // Trouver la classe de valeur correspondante
        if (x >= min_value && x < max_value)
        {
            classIndex = (int)((x - min_value) / ((max_value - min_value) / CAUCHY_N_CLASSES));
            if (classIndex >= 0 && classIndex < CAUCHY_N_CLASSES)
            {
                classes[classIndex]++;
            }
        }
    }

    // Écrire les résultats dans le fichier
    for (i = 0; i < CAUCHY_N_CLASSES; i++)
    {
        classCenter = min_value + (i + 0.5) * ((max_value - min_value) / CAUCHY_N_CLASSES);
        fprintf(file, "%lf %d\n", classCenter, classes[i]); // Écrire la classe et sa fréquence
    }

    fclose(file);
}
