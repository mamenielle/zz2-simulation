#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "lois.c"

int main()
{
    // -------------------- Initialisation du Générateur --------------------

    unsigned long long init[4] = {0x12345ULL, 0x23456ULL, 0x34567ULL, 0x45678ULL}; // Tab pour initialisation de MT
    int                      l = 4;                                                                     // Longueur du tableau

    init_by_array64(init, l);

    // --------------------------- Loi de Poisson ---------------------------

    double lambda = 5.0;    // Lambda de la loi de Poisson

    poissonSimulation(lambda, "poisson.txt");

    lambda = 10.0;

    poissonSimulation(lambda, "poisson 2.txt");

    // ---------------------------- Loi log-normale ---------------------------

    double mu = 0.0;
    double sigma = 1.0;

    simulationLogNormale(mu, sigma, "lognormale.txt");

    sigma = 0.25;

    simulationLogNormale(mu, sigma, "lognormale 2.txt");

    sigma = 10;

    simulationLogNormale(mu, sigma, "lognormale 3.txt");

    sigma = 0.5;

    simulationLogNormale(mu, sigma, "lognormale 4.txt");

    // ---------------------------- Loi de Cauchy ---------------------------

    double x0 = 0.0;    // Paramètre de localisation
    double gamma = 1.0; // Paramètre d'échelle

    simulationCauchy(x0, gamma, "cauchy.txt");

    // Exercice d'application

    x0 = 100.0;
    gamma = 5.0;

    simulationCauchy(x0, gamma, "cauchy 2.txt");

    return 0;
}
