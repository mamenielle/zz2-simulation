#define N_ECHANTILLONS       10000 // Nombre d'échantillons générés
#define POISSON_N_OCCURENCES 20    // Taille maximale du tableau occurrences
#define LOGNORMALE_N_CLASSES 50    // Nombre de classes pour la loi log-normale
#define CAUCHY_N_CLASSES     40    // Nombre de classes pour Cauchy
#define CAUCHY_LARGEUR       100   // Valeurs observées entre -100 et +100
#define M_PI 3.14159265358979323846

int poisson(double lambda);
void poissonSimulation(double lambda, char *fichier);
double log_normale(double mu, double sigma);
void simulationLogNormale(double mu, double sigma, char *fichier);
double cauchy(double x0, double gamma);
void simulationCauchy(double x0, double gamma, char *fichier);
