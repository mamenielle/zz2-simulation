#include <stdio.h>
#include <stdlib.h>
#include "mt19937-64.h"
#include "patient.h"

#define TAILLE_GRILLE 100
#define TAILLE_POPULATION 1240
patient grille[TAILLE_GRILLE][TAILLE_GRILLE];
float taux_contamination[11] = {0, 0, 0.6, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1};

void initMT()
{
    unsigned long long init[4] = {0x12345ULL, 0x23456ULL, 0x34567ULL, 0x45678ULL}; // Tab pour initialisation de MT
    int l = 4;
    init_by_array64(init, l);
}

void initGrille()
{
    int i, j;
    patient p;
    p.malade = '.';

    // initialiser la grille à 0
    for (i = 0; i < TAILLE_GRILLE; i++)
    {
        for (j = 0; j < TAILLE_GRILLE; j++)
        {
            grille[i][j] = p;
        }
    }
}

void remplirGrille(double pourcentageSain, double pourcentageContamine, int taille_population)
{
    int i, j;

    int rempli = 0; // Nombre de cases remplies

    double k;
    patient p;

    while (rempli < taille_population)
    {
        i = genrand64_int63() % TAILLE_GRILLE;
        j = genrand64_int63() % TAILLE_GRILLE;

        if (grille[i][j].malade == '.')
        {
            p.x = i;
            p.y = j;
            p.heure_maladie = 0;
            p.jour_maladie = 0;
            k = genrand64_real1();
            if (k < pourcentageContamine)
            {
                p.malade = 'c';
                grille[i][j] = p;
            }
            else if (k < pourcentageContamine + pourcentageSain)
            {
                p.malade = 's';
                grille[i][j] = p;
            }
            else
            {
                p.malade = 'i';
                grille[i][j] = p;
            }
            rempli++;
        }
    }
}

void afficherGrille()
{
    int i, j;
    for (i = 0; i < TAILLE_GRILLE; i++)
    {
        for (j = 0; j < TAILLE_GRILLE; j++)
        {
            printf("%c ", grille[i][j].malade);
        }
        printf("\n");
    }
    printf("\n");
}

void simulerUnDeplacement()
{
    int i, j;
    int new_i, new_j;
    int direction = 0;

    for (i = 0; i < TAILLE_GRILLE; i++)
    {
        for (j = 0; j < TAILLE_GRILLE; j++)
        {
            if (grille[i][j].malade != '.')
            {
                direction = genrand64_int63() % 4;

                new_i = i, new_j = j;

                // Déplacement vers le haut
                if (direction == 0)
                {
                    new_i = (i - 1 + TAILLE_GRILLE) % TAILLE_GRILLE;
                }
                // Déplacement vers le bas
                else if (direction == 1)
                {
                    new_i = (i + 1) % TAILLE_GRILLE;
                }
                // Déplacement vers la gauche
                else if (direction == 2)
                {
                    new_j = (j - 1 + TAILLE_GRILLE) % TAILLE_GRILLE;
                }
                // Déplacement vers la droite
                else if (direction == 3)
                {
                    new_j = (j + 1) % TAILLE_GRILLE;
                }

                // Vérifier que la nouvelle case est vide avant de déplacer
                if (grille[new_i][new_j].malade == '.')
                {
                    grille[new_i][new_j] = grille[i][j];
                    grille[i][j].malade = '.';
                    break;
                }
            }
        }
    }
}

void calculerContamination()
{
    int i, j, x, y;
    double prob_contamination; // Définir le taux comme une probabilité fractionnelle.

    for (i = 0; i < TAILLE_GRILLE; i++)
    {
        for (j = 0; j < TAILLE_GRILLE; j++)
        {
            if (grille[i][j].malade == 'c')
            {
                for (x = i - 1; x <= i + 1; x++)
                {
                    for (y = j - 1; y <= j + 1; y++)
                    {
                        // Calculer les indices dans l'espace torique
                        int torique_x = (x + TAILLE_GRILLE) % TAILLE_GRILLE;
                        int torique_y = (y + TAILLE_GRILLE) % TAILLE_GRILLE;

                        // Éviter d'infecter la cellule actuelle
                        if (!(torique_x == i && torique_y == j))
                        {
                            prob_contamination = taux_contamination[grille[i][j].jour_maladie];

                            // Si la cellule voisine est sensible ('s') et que l'infection se produit
                            if (grille[torique_x][torique_y].malade == 's' && genrand64_real1() < prob_contamination)
                            {
                                grille[torique_x][torique_y].malade = 'c';
                                grille[torique_x][torique_y].jour_maladie = 0;
                                grille[torique_x][torique_y].heure_maladie = 0;
                            }
                        }
                    }
                }

                // Incrémenter les compteurs de maladie pour la cellule infectée actuelle
                grille[i][j].heure_maladie++;
                if (grille[i][j].heure_maladie >= 24)
                {
                    grille[i][j].heure_maladie = 0;
                    grille[i][j].jour_maladie++;
                }

                // Vérifier si le malade doit revenir à l'état sensible
                if (grille[i][j].jour_maladie > 11)
                {
                    grille[i][j].malade = 's';
                    grille[i][j].jour_maladie = 0;
                    grille[i][j].heure_maladie = 0;
                }
            }
        }
    }
}

void simulerMois(int nbMois)
{
    int j, h;
    for (j = 0; j < 30 * nbMois; j++)
    {
        for (h = 0; h < 24; h++)
        {
            if (h > 6 && h < 24)
            {
                simulerUnDeplacement();
            }
            calculerContamination();
        }
    }
}

total analyserGrille()
{
    int j, k;
    total t;
    t.c = 0;
    t.s = 0;
    t.i = 0;
    for (j = 0; j < TAILLE_GRILLE; j++)
    {
        for (k = 0; k < TAILLE_GRILLE; k++)
        {
            switch (grille[j][k].malade)
            {
            case 's':
                t.s += 1;
                break;
            case 'c':
                t.c += 1;
                break;
            case 'i':
                t.i += 1;
                break;
            default:
                break;
            }
        }
    }
    return t;
}

total lancerSimulation(int nbExp, int nbMois, float pourcentage_sain, float pourcentage_contamine, int taille_population)
{
    int i;
    total *t = malloc(nbExp * sizeof(total));
    if (t == NULL)
    {
        perror("erreur malloc experience 1");
    }

    for (i = 0; i < nbExp; i++)
    {
        initGrille();
        remplirGrille(pourcentage_sain, pourcentage_contamine, taille_population);
        simulerMois(nbMois);
        t[i] = analyserGrille();
    }

    total resultats;
    resultats.s = 0;
    resultats.c = 0;
    resultats.i = 0;

    for (i = 0; i < nbExp; i++)
    {
        resultats.s += t[i].s;
        resultats.c += t[i].c;
        resultats.i += t[i].i;
    }

    // Calcul des moyennes
    resultats.s /= nbExp;
    resultats.c /= nbExp;
    resultats.i /= nbExp;

    free(t);

    return resultats;
}

void Experience1(int nbMois)
{
    int i;

    // Création d'un fichier qui contiendra les résultats
    char chaine[50];
    sprintf(chaine, "Expérience 1 - %d mois", nbMois);

    FILE *f = fopen(chaine, "w+");
    if (f == NULL)
    {
        perror("Erreur à l'ouverture du fichier");
    }

    // Initialisation
    initMT();

    float pourcentage_sain = 0.9;
    float pourcentage_contamine = 0.05;

    total resultats;

    for (i = 0; i < 9; i++)
    {
        resultats = lancerSimulation(10, nbMois, pourcentage_sain, pourcentage_contamine, TAILLE_POPULATION);
        fprintf(f, "%.2lf\n", resultats.c);
        pourcentage_sain -= 0.1;
        pourcentage_contamine += 0.1;
    }

    fclose(f);
}

void Experience2(int nbMois)
{
    int i;

    // Création d'un fichier qui contiendra les résultats
    char chaine[50];
    sprintf(chaine, "Expérience 2 - %d mois", nbMois);

    FILE *f = fopen(chaine, "w+");
    if (f == NULL)
    {
        perror("Erreur à l'ouverture du fichier");
    }

    // Initialisation
    initMT();
    float pourcentage_sain = 0.55;
    float pourcentage_contamine = 0.4;

    total resultats;

    for (i = 1000; i <= 10000; i += 1000)
    {
        resultats = lancerSimulation(10, nbMois, pourcentage_sain, pourcentage_contamine, i);
        fprintf(f, "%.2lf\n", resultats.c);
    }

    fclose(f);
}

void Experience3(int nbMois)
{
    int i;

    // Création d'un fichier qui contiendra les résultats
    char chaine[50];
    sprintf(chaine, "Expérience 3 - %d mois", nbMois);

    FILE *f = fopen(chaine, "w+");
    if (f == NULL)
    {
        perror("Erreur à l'ouverture du fichier");
    }

    // Initialisation
    initMT();
    float pourcentage_sain = 0.55;
    float pourcentage_contamine = 0.4;

    total resultats;

    for (i = 1000; i <= 10000; i += 1000)
    {
        resultats = lancerSimulation(10, nbMois, pourcentage_sain, pourcentage_contamine, i);
        fprintf(f, "%.2lf\n", resultats.c);
    }

    fclose(f);
}

int main()
{
    // Experience1(1);
    // Experience1(3);
    // Experience1(6);
    // Experience1(12);
    Experience2(1);
    Experience2(3);
    Experience2(6);
    Experience2(12);

    return 0;
}